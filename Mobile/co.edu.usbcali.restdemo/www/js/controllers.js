angular.module('app.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('GetCtrl', function($scope, $http, $location, $stateParams, _APICONFIG) {

  $scope.sum = {a:0,b:0};
  $scope.sumar = function(){

    $http.get(_APICONFIG.url+"index/suma"+_APICONFIG.key+"&a="+$scope.sum.a+"&b="+$scope.sum.b)
      .then(function(response) {
        var r = response.data.resultado;
        $scope.resultado = r;
    });
  };

  $scope.users = {};
  $scope.getUsers = function(){

    $http.get(_APICONFIG.url+"Users/"+_APICONFIG.key)
      .then(function(response) {
        $scope.users = response.data;
    });
  };

})

.controller('PostCtrl', function($scope, $http, $location, $stateParams, _APICONFIG, serializer) {
  $scope.daUser = { id:null, username: null, password: null, email: null };
  $scope.postUser = function(){

    var data = {
      id:null,
      username: $scope.daUser.username,
      password: $scope.daUser.password,
      email: $scope.daUser.email
    };
    $http({
      url:_APICONFIG.url+"Users/"+_APICONFIG.key,
      method: "POST",
      data: serializer.prepareData(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).then(function(response) {
      console.log(response.data);
    });

  };
})

.controller('PutCtrl', function($scope, $http, $location, $stateParams, _APICONFIG) {

  $scope.uCount = 0;

  $scope.search = {
    label: "Username",
    placeholder: "John",
    value: null
  };

  $scope.list = [
      {option: "Username", value:0, placeholder:"Jhon"},
      {option: "Password", value:1, placeholder:"1234"},
      {option: "Email", value:2, placeholder:"some@email.com"}
    ];

  $scope.selected = $scope.list[0].value;

  $scope.update = function(value){
          $scope.search.label = $scope.list[value].option;
          $scope.search.placeholder = $scope.list[value].placeholder;
  };

  $scope.users = {};

  $scope.getUser = function(){

    $http.get(_APICONFIG.url+"Users/find/"+_APICONFIG.key+"&field=username&value="+$scope.search.value)
      .then(function(response) {
        console.log(response);
        $scope.users = response.data;
        $scope.uCount = response.data.length;
    });
  };

  $scope.editThis = function(user){
    $location.path("/app/putUser").search({user:user});
  };

})

.controller('PutUsrCtrl', function($scope, $http, $location, $stateParams, _APICONFIG, serializer) {
  var params = $location.search();
  $scope.user = params.user;

  $scope.update = function(){

    var data = {
      id:$scope.user.id,
      username: $scope.user.username,
      password: $scope.user.password,
      email: $scope.user.email
    };

    $http({
      url:_APICONFIG.url+"Users/"+_APICONFIG.key,
      //url: "http://localhost/Interoperabilidad/Server/putTest.php",
      method:"PUT",
      //data: data,
      data: serializer.prepareData(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },transformResponse: [function (data) {
          console.log(data);
      }]
    })/*.then(function(response) {
      console.log(response);
    });*/

  };

})

.controller('DeleteCtrl', function($scope, $http, $location, $stateParams) {

})

.controller('SearchCtrl',function($scope,$http,$location,serializer){

});
