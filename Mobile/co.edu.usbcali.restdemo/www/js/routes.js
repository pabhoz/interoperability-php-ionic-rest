angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

  .state('app.get', {
    url: '/get',
    views: {
      'menuContent': {
        templateUrl: 'templates/getRequests.html',
        controller: 'GetCtrl'
      }
    }
  })

  .state('app.post', {
    url: '/post',
    views: {
      'menuContent': {
        templateUrl: 'templates/postRequests.html',
        controller: 'PostCtrl'
      }
    }
  })

  .state('app.put', {
    url: '/put',
    views: {
      'menuContent': {
        templateUrl: 'templates/putRequests.html',
        controller: 'PutCtrl'
      }
    }
  })
  .state('app.putUser', {
    url: '/putUser',
    views: {
      'menuContent': {
        templateUrl: 'templates/putUser.html',
        controller: 'PutUsrCtrl'
      }
    }
  })

  .state('app.delete', {
    url: '/delete',
    views: {
      'menuContent': {
        templateUrl: 'templates/deleteRequests.html',
        controller: 'DeleteCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/get');
});
