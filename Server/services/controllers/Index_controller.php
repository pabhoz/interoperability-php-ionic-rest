<?php

class Index_controller extends Controller {

	function __construct() {
		parent::__construct();
	}

        public function getIndex(){
            Request::setHeader(202,"text/html");
            echo "Get method Index controller";
        }

        public function postIndex(){
            Request::setHeader(202,"text/html");
            echo "Post method Index controller";
        }

        public function getSuma($a,$b){
            if (!isset($a) || !isset($b)) {
                throw new Exception('Paremetros insuficientes.');
            }
            Request::setHeader(200,"text/plain");
            $r["resultado"] = $a + $b;
						echo json_encode($r);
        }
}
