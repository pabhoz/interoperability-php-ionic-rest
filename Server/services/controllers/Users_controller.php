<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users_controller
 *
 * @author pabhoz
 */
class Users_controller extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getUsers($id){

        if($id){
            $usr = Usuario::getById($id);
            if($usr == null){exit("{}");}
            print_r(json_encode($usr->toArray()));
        }else{
            print_r(json_encode(Usuario::getAll()));
        }
    }

    public function getFind($field,$value){

      $usr = Usuario::like($field,$value);
      echo json_encode($usr);

    }

    public function postUsers(){
        $keys = Usuario::getKeys();
        unset($keys[0]);
        $this->validateKeys($keys, filter_input_array(INPUT_POST));
        $usr = Usuario::instanciate($_POST);
        $r = $usr->create();
        if($r["error"] == 0){ $status = 201; }else{ $status = 400; }
        Request::setHeader($status);
        print(json_encode($r));
    }

    public function putUsers() {

      $_PUT = $this->_PUT;

      if(!isset($_PUT["id"]) || empty($_PUT["id"])){
        Request::setHeader("400");
        exit();
      }

        $usr = Usuario::getById($_PUT["id"]);

        foreach ($_PUT as $key => $value) {
          $usr->{"set" . ucfirst($key)}($value);
        }
        $r = $usr->update();

        if ($r["error"] == 0) {
            $msg = $r["msg"];
            $args = ["user" => Penelope::arrayToJSON($usr->toArray())];
            $response = Request::response($msg, $args, $r["error"]);
            echo Penelope::arrayToJSON($response);
        } else {
            Request::setHeader("400");
        }
        
    }
}
